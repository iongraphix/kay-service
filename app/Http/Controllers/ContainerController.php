<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class ContainerController extends Controller
{
    //
    public function getInfo($id){

    	$api = new \APIClient([
		    'base_uri'   => 'http://0.0.0.0:5000',
		    'api_key'    => 'youtube'
		]);
		$container = $api->containers->get($id);
		return response()->json($container);
    }
}
