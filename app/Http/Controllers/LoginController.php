<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;

class LoginController extends Controller
{
    
   public function normal_login(Request $request){
         $credentials = $request->only('email', 'password');
         Auth::logout();
         if(Auth::attempt($credentials)){
            $user = Auth::user();
            $user->role;
            return response()->json(array("status" => "success", "info" => $user));
         }
         else {

            return response()->json(array("status" => "error"));
         }
    }
    function normal_logout(Request $request){
        Auth::logout();
        return response()->json(array("status" => "success", "info" => "Logout Done!"));
    }

}
