<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;

class UploadController extends Controller
{
    public function user(Request $request){
    	$credentials = $request->only('id', 'image');
    	$user  = User::find($credentials["id"]);
        $user->thumbnail = $credentials["image"];
        $nu = $user->save();
        return response()->json(array("status" => "success", "info" => $nu));
    }
}
