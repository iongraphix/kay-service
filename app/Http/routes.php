<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/auth', 'LoginController@normal_login');
Route::get('/logout', 'LoginController@normal_logout');
Route::post('/upload/user', 'UploadController@user');
Route::get('/container/{id}', 'ContainerController@getInfo');


Route::group(['prefix' => '/resource'], function () {
    
    // Role related urls
    Route::resource('role', 'RolesController');

    // User related urls
    Route::resource('user', 'UserController');


});


