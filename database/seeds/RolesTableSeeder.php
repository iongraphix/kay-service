<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $roles = array(
                ['id' => 1, 'title' => 'Teacher'],
                ['id' => 666, 'title' => 'Student'],
        );

        foreach ($roles as $role)
        {
            Role::create($role);
        }
    }
}
